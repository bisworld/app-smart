<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Product;
use Illuminate\Support\Facades\Http;
use App\Services\Product as ProductService;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductTest extends TestCase
{
    use RefreshDatabase;

    const URL = 'api/products';

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
     * Product fetched test.
     *
     * @return void
     */
    public function testFetch()
    {
        $products = Product::factory()->count(3)->categories()->make();

        Http::fake([
            ProductService::URL . '*' => Http::response([
                'products' => $products->toArray()
            ], 200),
        ]);

        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
        ])->json('GET', self::URL, ['page' => 1]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'items' => $products->toArray(),
                'pages' => 1,
            ]);
    }

    /**
     * Product created success.
     *
     * @return void
     */
    public function testCreated()
    {
        $product = Product::factory()->categories()->make();

        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
        ])->json('POST', self::URL, $product->toArray());

        $this->assertDatabaseHas('products', [
            'id' => $product->id,
        ]);

        $this->assertDatabaseCount('categories', 3);

        $response
            ->assertStatus(200)
            ->assertJson([
                'message' => 'Product Saved!',
            ]);
    }
}

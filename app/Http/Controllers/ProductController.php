<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\ProductStore;
use App\Services\Product as ProductService;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param ProductService $service
     * @return JsonResponse
     */
    public function index(ProductService $service)
    {
        return response()->json([
            'items' => $service->getProducts(),
            'pages' => $service->getPages(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductStore $request
     * @return JsonResponse
     */
    public function store(ProductStore $request)
    {
        DB::transaction(function () use ($request) {
            $product = Product::updateOrCreate(
                $request->only('id'),
                $request->only(['image_url', 'product_name'])
            );

            $ids = collect(explode(',', $request->get('categories')))
                ->map(function (string $category) {
                    return Category::firstOrCreate([
                        'name' => trim($category)
                    ])->id;
                });

            $product->categories()->sync($ids);
        });

        return response()->json([
            'message' => 'Product Saved!'
        ]);
    }
}

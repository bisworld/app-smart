<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'           => ['required', 'numeric'],
            'image_url'    => ['string', 'max:200'],
            'product_name' => ['required', 'string', 'max:100'],
            'categories'   => ['string', 'max:1000']
        ];
    }
}

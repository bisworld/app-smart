<?php

namespace App\Services;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\Response;

class Product
{
    /**
     * Get products url
     */
    const URL = 'https://world.openfoodfacts.org/cgi/search.pl';

    /**
     * Fields to front
     */
    const FIELDS = [
        'id', 'image_thumb_url', 'image_url', 'product_name', 'categories'
    ];

    /**
     * @var Response
     */
    private $response;

    /**
     * Product constructor.
     */
    public function __construct()
    {
        $this->response = Http::get(self::URL, [
            'action'       => 'process',
            'sort_by'      => 'unique_scans_n',
            'page_size'    => 20,
            'json'         => 1,
            'page'         => (int)request()->get('page', 1),
            'search_terms' => (string)request()->get('search'),
        ])->throw();
    }

    /**
     * @return Collection
     */
    public function getProducts(): Collection
    {
        return collect($this->response->json('products'))
            ->map(function ($item) {
                return collect($item)->only(self::FIELDS);
            })->reject(function ($item) {
                return empty($item['id']);
            });
    }

    /**
     * @return int
     */
    public function getPages(): int
    {
        $count = (int)$this->response->json('count', 1);
        $size  = (int)$this->response->json('page_size', 20);

        return (int) ceil($count / $size);
    }
}

<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id'           => $this->faker->unique()->numberBetween(100, 100000),
            'image_url'    => $this->faker->imageUrl(),
            'product_name' => $this->faker->name,
        ];
    }

    /**
     * Indicate that the user is suspended.
     *
     * @return Factory
     */
    public function categories()
    {
        return $this->state(function () {
            return [
                'categories' => Category::factory()->count(3)->make()->pluck('name')->join(', '),
            ];
        });
    }
}
